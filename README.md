# Books for Inform 7
Copyright (c) 2011 Bart Massey

This [Inform 7](http://inform7.com) extension tries to
provide some basic support for books and reading. It could
use a lot of help.

Released under the Creative Commons Attribution 3.0 United
States (CC BY 3.0) license. See the file `COPYING` in this
distribution for license details. Creative Commons provides
a nice
[description](http://creativecommons.org/licenses/by/3.0/us/)
of the license.
