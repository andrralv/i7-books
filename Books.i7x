Version 2 of Books by Bart Massey begins here.

Include Basic Literacy By Bart Massey

[Copyright © 2011-2017 Bart Massey and André Rodriguez]

[Released under the Creative Commons Attribution 3.0 United States (CC BY 3.0) license. See the file COPYING provided with distributions of this work for license details.]

[Thanks to Roger, Zarf and DavidW on ifMUD for helping Bart out when stuck 2011-12-29; Zarf pointed out about "the item described".]

Book - Books and Book Parts

A book part is a kind of thing. A spine is a kind of book part. A front cover is a kind of book part. A back cover is a kind of book part.
Understand "title" as a spine. Understand "bookcover" as the front cover. Understand "cover" as the front cover. Does the player mean reading the front cover: It is likely.

A book is a kind of container. It is openable. It is usually closed. A spine, a front cover, and a back cover are part of every book. A book has some text called the title. A book has some text called the back cover text. A book has some text called the front cover text. A book can be bookmarked.

Part - Book Spines

To say the titled book info (this is the say the titled book info rule):
	say "[the title of the holder of the noun][paragraph break]";

To say the untitled book info (this is the say the untitled book info rule):
	say "[the noun] has no title.[paragraph break]".

Check a player reading a spine (this is the reading a spine rule):
	if the title of the holder of the noun is not "":
		Instead say the titled book info;
	otherwise:
		Instead say the untitled book info.

Part - Book Covers

To say the book is nondescript info (this is the say the book is nondescript info rule):
	say "[the noun] is nondescript.[paragraph break]".
	
To say the front cover info (this is the say the front cover info rule):
	say "[the front cover text of the holder of the noun][paragraph break]".
	
To say the back cover info (this is the say the back cover info rule):
	say "[the back cover text of the holder of the noun][paragraph break]".
		
Instead of a player reading a front cover (this is the reading a front cover rule):
	If the front cover text of the holder of the noun is not "":
		say the front cover info;
	otherwise: 
		say the title of the holder of the noun.
			
Instead of a player reading a back cover (this is the reading a back cover rule):
	if the back cover text of the holder of the noun is "":
		say the book is nondescript info;
	otherwise: 
		say the back cover info.
		
Understand "of [any book]" as a book part.

Part - Book Titles

The title of a book is usually "[the printed name in title case]". 

Definition: a book is titled if the title of it is not empty. Definition: a book is untitled if the title of it is empty.

Part - Implicit Opening / Closing

To implicitly open (the book opened - a book) (this is the implicitly open a book rule): silently try opening the book opened; say first opening the book opened clarification.

To say first opening (the book opened - a book) clarification (this is the say first opening clarification rule): say "(first opening [the book opened]) [command clarification break]".

To say finally closing (the book closed - a book) clarification (this is the say finally closing clarification rule): say "(finally closing [the book closed]) [command clarification break]".

To implicitly close (the book closed - a book) (this is the implicitly close a book rule): silently try closing the book closed; say finally closing the book closed clarification.

Before an actor searching a closed book (called the book searched) (this is the open closed book for searching rule): implicitly open the book searched.

Part - Pages

Some pages are a kind of plural-named thing. Some pages can be writable. The description is usually "[if the page count of the book abstracted by the item described is greater than 0][The book abstracted by the item described] has [the page count of the book abstracted by the item described] pages. [end if]The pages are uninteresting in themselves; you may want to try reading them." Some pages are part of every book.

To decide what book is the book abstracted by (the concrete pages - pages) (this is the find the book abstracting pages rule): decide on a random book incorporating the concrete pages.

A book has a table name called the manuscript.

A book has a number called the current page. The current page of a book is usually 0.

Before an actor opening a closed book (this is the open a book to the first page rule): now the current page is 1.
After an actor closing a book (this is the close a book and lose the place rule): now the current page is 0.

Table of nonexistent pages
Page Text
a text

Section - Page Functions

To decide what text is the current page text of (the book read - a book) (this is the decide the current page text rule): let M be the manuscript of the book read; let N be the current page of the book read; decide on the Page Text in row N of M.

To decide which number is the page count of (the book in question - a book) (this is the decide the page count of a book rule): Let M be the manuscript of the book in question; if M is not empty, decide on the number of rows in M; otherwise decide on 0. 

To say a page of (the book read - a book) (this is the say a page of a book rule): Let N be the page count of the book read; if N is greater than 1, say the current page number clarification of the book read; say the current page text of the book read; increment the current page of the book read.

To say the current page number clarification of (the book read - a book) (this is the say the current page number clarification rule): say "(page [current page of the book read])[line break]".

To say the current page text of (the book read - a book) (this is the say the current page text rule): say "[line break][current page text of the book read][paragraph break]".

Section - Turning To

Turning it to is an action applying to one thing and one value and requiring light. Understand "page [number]" or "[number]" as "[page number]". Understand "turn [book] to [page number]" or "open [book] to [page number]"  as turning it to. Understand "turn to [page number] in [book]" or "open to [page number] in [book]" as turning it to (with nouns reversed).

Before an actor turning a closed book (called the book turned) to a number (this is the implicitly open a closed book for turning to rule): implicitly open the book turned.

Check an actor turning a book (called the book turned) to a number (called the target page) (this is the check turning to a valid page rule): If the target page is less than 1 or the target page is greater than the page count of the book turned, instead say error the target page does not exist in the book turned.

To say error (the target page - a number) does not exist in (the book turned - a book) (this is the say nonexistent target page error rule): if the target page is less than 1, instead say "[The book turned] starts at page 1."; say "[The book turned] has [the page count of the book turned] pages: page [the target page] is not one of them."

Carry out an actor turning a book (called the book turned) to a number (called the page number) (this is the turn a book to a page rule): now the current page of the book turned is the page number; say "You turn to page [page number] of [the book turned]."

Section - Turning To First Or Last

Turning to front is an action applying to one thing and requiring light. Understand "front" or "the front" or "front page" or "front page" as "[front]". Understand "turn [book] to [front]" or "turn to [front] of/in [book]" as turning to front. Instead of an actor turning to front a book (called the book turned) (this is the turn to front page rule): try turning the book turned to 1.

Turning to last is an action applying to one thing and requiring light. Understand "last/end/ending/final" or "the last/end/ending/final" or "last/end/ending/final page" or "the last/end/ending/final page" as "[last]". Understand "turn [book] to [last]" or "turn to [last] of/in [book]" as turning to last. Instead of an actor turning to last a book (called the book turned) (this is the turn to last page rule): try turning the book turned to the page count of the book turned.

Section - Summarizing

Summarizing is an action applying to one visible thing and requiring light. Understand "summarize [book]" as summarizing.

Before an actor summarizing a closed book (called the book to be summarized) (this is the implicitly open a book before summarizing rule): implicitly open the book to be summarized.

Check an actor summarizing a book (called the book to be summarized) (this is the check summarizing a book rule): If the read text of book to be summarized is empty, instead say error the book to be summarized has no summary.

To say error (the book to be summarized - a book) has no summary (this is the say summarizing an unsummarizable book error rule): say "[The book to be summarized] is not easily summarized---another possibility is to try READing it."

Carry out an actor summarizing a book (called the book summarized) (this is the summarize a book rule): say the read text of the book summarized.

Understand "read description of [something]" as summarizing. 

Section - Book Reading

Before an actor reading a closed book (called the book read) (this is the open closed book for reading rule): implicitly open the book read.

Check an actor reading a book (called the book read) (this is the check reading a book rule): if the book read is closed, instead say error the book read is stuck closed; let N be the page count of the book read; if N is 0 and the read text of the book read is empty, instead say error the book read has no content; let P be the current page of the book read; if N is 1 and P is greater than 1, now the current page of the book read is 1; if N is greater than 1 and P is greater than N, instead say error the book read has no more pages.

The check reading a thing rule is not listed in the check reading rulebook.

Check an actor reading a thing (called the thing read) (this is the check reading a thing including books rule): if the thing read is not a book, abide by the check reading a thing rule. 

To say error (the book read - a book) has no content (this is the say reading an no-content book error rule): say "The contents of [the book read] are uninteresting."

To say error (the book read - a book) is stuck closed (this is the say book stuck closed error rule): say "[The book read] seems to be stuck closed, and cannot be read." [XXX: It would take some finagling for this rule to be reached. It's just defensive programming.]

To say error (the book read - a book) has no more pages (this is the say reading past the end of a book rule): say "[The book read] has no more pages."

Carry out an actor reading a book (called the book read) (this is the read a book rule): if the page count of the book read is greater than 0, say a page of the book read; otherwise say the read text of the book read; rule succeeds.

To finish (the book read - a book): implicitly close the book read; now the current page of the book read is 1.

After an actor reading an open book (called the book read) (this is the reading the last page of a book rule): let N be the page count of the book read; if N is greater than 1 and the current page of the book read is greater than N, finish the book read.

Book - The Relevant Book

To say error not obvious choice of book (this is the say error not obvious choice of book rule):
	say "There is no obvious choice of book.".

To say the currently reading book info (this is the say the currently reading book info rule):
	say "(reading [the noun])[command clarification break]".

The nonexistent book is a privately-named book. The last book touched is a book variable. The last book touched is the nonexistent book.

Before an actor doing something to a book (called the book touched) (this is the remember the last book touched rule): now the last book touched is the book touched.

To say error cannot find a relevant book (this is the say cannot find a relevant book error rule):
	say error not obvious choice of book.

To decide which book is the relevant book: If the last book touched is not the nonexistent book and the last book touched is visible, decide on the last book touched; if exactly one book is visible, decide on a random visible book; say error cannot find a relevant book; decide on the nonexistent book.

Understand "read" or "read page" as reading. Rule for supplying a missing noun when reading (this is the find a book to read rule): Let B be the relevant book; if B is the nonexistent book, rule fails; now the noun is B; say the currently reading book info.

Part - Turning The Page

Turning to page is an action applying to one value and requiring light. Understand "turn to page [number]" or "open to page [number]" as turning to page.

Carry out turning to page number (called the page number turned to) (this is the turn to page rule): Let B be the relevant book; if B is the nonexistent book, rule fails; try turning B to the page number turned to.

Page turning is an action requiring light. Understand "turn page" or "turn the page" as page turning. 

Page backturning is an action requiring light. Understand "turn page back" or "turn back the page" or "turn back" as page backturning. 

To say error there arent any pages in book (this is the say error there arent any pages in book rule):
	say "There aren't any more pages in [the relevant book][paragraph break]";
	say "(Closing [the relevant book])"

Carry out page backturning (this is the page backturning rule): 
	Let B be the relevant book; 
	if B is the nonexistent book:
		rule fails; 
	if the current page of B is greater than 2:
		now the current page of B is the current page of B - 2; 
		try reading the relevant book; 
	otherwise:
		try closing the relevant book;
		say error there arent any pages in book.


Carry out page turning (this is the turn the page of the relevant book rule): Try reading the relevant book.

Part - Book Writing

Carry out an actor writing on a book (called the current book) (this is the writing on a book rule):
	let P be the current page of the relevant book;
	let PI be P - 1;
	let M be the manuscript of the relevant book;
	let T be the Page Text in row PI of M;
	let PA be the topic understood;
	now the Page Text in row PI of M is "[T][paragraph break][PA]";

To say error opening (the book written - a book) (this is the say error opening the written book rule):
	say "You must open [the book written] before you can write in it.".
	
Check an actor writing on a book (called the book written) (this is the cannot write in a closed book rule):
	If the book is closed, instead say error opening the book written.
	
Part - Page Erasing

Page erasing is an action requiring light. Understand "erase page" as page erasing.

Carry out page erasing (this is the erasing a page rule):
	let P be the current page of the relevant book;
	let PI be P - 1;
	let M be the manuscript of the relevant book;
	let T be the Page Text in row PI of M;
	let PA be the topic understood;
	now the Page Text in row PI of M is "[T][paragraph break][PA]";

Part - Bookmarks

A bookmark is a kind of book part. A bookmark can be carried by the player. A bookmark has a number called the marked page.

A book is rarely bookmarked. A book is usually transparent.

Section - Bookmarking Actions

Before an actor inserting a bookmark into a closed book (called the book inserted into) (this is the open closed books for bookmarking rule): implicitly open the book inserted into.

Check an actor inserting a thing (called the thing inserted) into a book (called the book inserted into) (this is the check inserting a thing into a book rule): If the thing inserted is not a bookmark, instead say error inserting the thing inserted that is not a bookmark into the book inserted into; if the page count of the book inserted into is 0, instead say error inserting the thing inserted into the book inserted into that has no pages. 

To say error inserting (the thing inserted - a thing) into (the book inserted into - a book) that has no pages (this is the say inserting into a book with no pages error rule): say "[The thing inserted] has no useful place in [the book inserted into]."

To say error inserting (the thing inserted - a thing) that is not a bookmark into (the book inserted into - a book) (this is the say inserting a non-bookmark into a book error rule): say "Putting [the thing inserted] into [the book inserted into] seems pointless."

After an actor inserting a bookmark (called the bookmark inserted) into a book (called the book inserted into) (this is the bookmark a book rule): now the marked page of the bookmark inserted is the current page of the book inserted into - 1; say report that the bookmark inserted is in the book inserted into; Now the book inserted into is bookmarked.

To say report that (the bookmark inserted - a bookmark) is in (the book inserted into - a book) (this is the say inserted a bookmark into a book report rule): say "[The bookmark inserted] falls gently into [the book inserted into]."

Understand "put [bookmark] at/in [page number]"  and "put [bookmark] at/in page [page number]" as a mistake ("To bookmark a particular page of some book, you need to open the book to that page, then insert the bookmark. Try OPEN (book) TO PAGE (page), INSERT (bookmark) INTO IT.").

Part - Opening To

Opening to is an action applying to one thing. Understand "turn to [bookmark]"  or "open to [bookmark]" as opening to.

Check an actor opening to a bookmark (called the target bookmark) (this is the check opening to a bookmark rule): If the target bookmark is not contained in a book, instead say error opening to the target bookmark that is not in a book.

To say error opening to (the target bookmark - a bookmark) that is not in a book (this is the opening to a bookmark not in a book error rule): say "[The target bookmark] is not in a book."

To decide which book is the marked book of (the target bookmark - a bookmark) (this is the decide the marked book of a bookmark rule): if a book (called B) contains the target bookmark, decide on B; otherwise decide on nothing.

Carry out an actor opening to a bookmark (called the target bookmark) (this is the open to a bookmark rule):  Let B be the marked book of the target bookmark; let P be the marked page of the target bookmark; try turning B to P.

To say clarification turning to page (the target page - a number) of (the book turned - a book) (this is the say turning to page clarification rule): say "(turning to page [the target page] of [the book turned])[command clarification break]"

Books ends here.

---- Documentation ----
This extension requires 'Basic Literacy' by Bart Massey. Basic Literacy has the read, write and erase commands required for books to work. (This story automatically imports Basic Literacy once it is installed.) The Basic Literacy extension separates the command 'read' from 'examine' as default by Inform7 standard rules. 

Books are readable things made of pages and book parts. Bookmarks are things that can be inserted inside books to save the last page read.

To use this extension: create a book and give it a default text, title and a manuscript (this is a table with rows that work as pages). Additionally you can add front cover text, back cover text or spine text to replace the defaults.

To use bookmarking create a bookmark. A bookmark can be placed in an open book.

You may make books separately writable and erasable. For example: "The Player's Diary is a writable erasable book in the X room."

To write on a book: open the book and go to the page you want to write on, then 'write ''text you want to write'''. 
To erase the contents of a page: go to the page you want to erase, then enter 'erase page'.

The available book parts are: {Spine, Front Cover, Back Cover and Title}.

The available commands for this extension are: {'X' is a book, read 'X', examine 'X', turn page, turn page back, go to 'Y' page of 'X', put bookmark, open 'X', close 'X', summarize 'X', read front cover of 'X', read back cover of 'X', read spine of 'X', read title of X', erase 'Book name', erase page.

Example: * Batster and Redbreast.

	*: "Batster and Redbreast Comic Book" by "Andre Rodriguez"

	Include Books by Bart Massey
	
	The Minimalist Room is a room. "This room is depressingly devoid of furniture and adornment."
	
	[The zorkmid coin: example of readable thing that isn't a book]
	The zorkmid coin is carried by the player. The description is "This large ugly coin purports to be worth exactly one Zorkmid. Whatever that is." The read text is "One Zorkmid[paragraph break]In Focom We Trust".

	The comic book is a book in the Minimalist Room. "[The item described] is lying on the floor. It looks like it's falling apart." The description is "A Batster and Redbreast comic book". The title is "Batster And Redbreast - Issue #53".  The front cover text is "1956. AC Comics". The back cover text is "'That Goofball is always causing trouble.' - Comic Revue". The read text is "This comic book only has about [the page count of the comic book - 1] pages in it." The manuscript is the Table of comic book pages.

	Table of comic book pages
	Page Text
	"Batster punches some random person."
	"Random person turns out to be Redbreast."
	"Batster apologizes and takes Redbreast for some ice cream."
	"Redbreast says he didn't like the ice cream."
	"Redbreast takes off his mask and turns out to be The Goofball."
	"Batster punches The Goofball right in the face."
	"The Goofball says: 'Wow, that was totally uncalled for.'"
	
	The awesome cardboard bookmark is a bookmark carried by the player. The description is "This [item described] has cool pictures on it."

	Test me with "read front cover of comic book / read comic book / turn page / turn page / put awesome bookmark in comic book / close comic book / read spine of comic book / read back cover of comic book / open comic book / turn to bookmark / turn to first page / close comic book / summarize comic book".
	
Example: * The Self Referential Folio - A simple book.

	*: "The Self-Referential Folio" by "Bart Massey"
	
	Include Books by Bart Massey
	
	The Minimalist Room is a room. "This room is depressingly devoid of furniture and adornment."
	The black pen is a writing tool in the minimalist room.

	The self-referential folio is a book in the Minimalist Room. "[The item described] is lying against a wall, looking a little thin." The title is "Title of Self-Referential Folio". The read text is "This book contains a single page describing its own content." The manuscript is the Table of Self-Referential Folio Pages.

	The recursive bookmark is a bookmark carried by the player. The description is "This bookmark has a picture of this bookmark on it."

	Table of Self-Referential Folio Pages
	Page Text
	"This sentence is the only sentence of this folio, one which happens to describe the entire content of this folio in a fashion that could be easily summarized"
	
	Test me with "read folio / read front cover of folio / summarize folio"
